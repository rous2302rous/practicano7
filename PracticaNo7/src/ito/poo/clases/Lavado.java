package ito.poo.clases;

import java.time.LocalDate;

public class Lavado extends Maquinas{

	private float capacidad;
	private float tiempodelavado;
	
	public Lavado(String descripcion, LocalDate fecha, float costo, float capacidad, float tiempodelavado) {
		super(descripcion, fecha, costo);
		this.capacidad = capacidad;
		this.tiempodelavado = tiempodelavado;
	}

	public float costolavado(float costo, float tiempo) {
		
		float porminuto=0, maquina;
		porminuto = 60/tiempo;
		
		maquina = costo/(100/0.5f);
		
		return maquina/porminuto;
	}
	
	public float getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(float capacidad) {
		this.capacidad = capacidad;
	}

	public float getTiempodelavado() {
		return tiempodelavado;
	}

	public void setTiempodelavado(float tiempodelavado) {
		this.tiempodelavado = tiempodelavado;
	}

	@Override
	public String toString() {
		return "Lavado [capacidad=" + capacidad + ", tiempodelavado=" + tiempodelavado + "]";
	}
	
	
	
}
