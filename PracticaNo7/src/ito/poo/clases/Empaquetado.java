package ito.poo.clases;

import java.time.LocalDate;

public class Empaquetado extends Maquinas{

	private String tipodeEmpaque;
	private float EmpaquesMinuto;
	
	public Empaquetado(String descripcion, LocalDate fecha, float costo, String tipodeEmpaque, float empaquesMinuto) {
		super(descripcion, fecha, costo);
		this.tipodeEmpaque = tipodeEmpaque;
		EmpaquesMinuto = empaquesMinuto;
	}

	public float costoempaque(float costo, float empaques) {
		
		float maquina;
		
		maquina = costo/(100/0.06f);
		
		return maquina/empaques;
	}
	
	public String getTipodeEmpaque() {
		return tipodeEmpaque;
	}

	public void setTipodeEmpaque(String tipodeEmpaque) {
		this.tipodeEmpaque = tipodeEmpaque;
	}

	public float getEmpaquesMinuto() {
		return EmpaquesMinuto;
	}

	public void setEmpaquesMinuto(float empaquesMinuto) {
		EmpaquesMinuto = empaquesMinuto;
	}

	@Override
	public String toString() {
		return "Empaquetado [tipodeEmpaque=" + tipodeEmpaque + ", EmpaquesMinuto=" + EmpaquesMinuto + "]";
	}
	
	
	
}
