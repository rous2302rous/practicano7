package ito.poo.clases;

import java.time.LocalDate;

public class Maquinas {

	private String Descripcion;
	private LocalDate fecha;
	private float Costo;
	
	public Maquinas(String descripcion, LocalDate fecha, float costo) {
		super();
		Descripcion = descripcion;
		this.fecha = fecha;
		Costo = costo;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public float getCosto() {
		return Costo;
	}

	public void setCosto(float costo) {
		Costo = costo;
	}

	@Override
	public String toString() {
		return "Maquinas [Descripcion=" + Descripcion + ", fecha=" + fecha + ", Costo=" + Costo + "]";
	}
	
	
	
}
