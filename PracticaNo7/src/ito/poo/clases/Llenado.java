package ito.poo.clases;

import java.time.LocalDate;

public class Llenado extends Maquinas{

	private float EnvasesporMinuto;
	private float RegulacionMililitros;
	
	public Llenado(String descripcion, LocalDate fecha, float costo, float envasesporMinuto,
			float regulacionMililitros) {
		super(descripcion, fecha, costo);
		EnvasesporMinuto = envasesporMinuto;
		RegulacionMililitros = regulacionMililitros;
	}

	public float costollenado(float costo, float envasesporMinuto){
		
		float maquina;
		
		maquina = costo/(100/0.25f);
		
		return maquina/envasesporMinuto;
	}
	
	public float getEnvasesporMinuto() {
		return EnvasesporMinuto;
	}

	public void setEnvasesporMinuto(float envasesporMinuto) {
		EnvasesporMinuto = envasesporMinuto;
	}

	public float getRegulacionMililitros() {
		return RegulacionMililitros;
	}

	public void setRegulacionMililitros(float regulacionMililitros) {
		RegulacionMililitros = regulacionMililitros;
	}

	@Override
	public String toString() {
		return "Llenado [EnvasesporMinuto=" + EnvasesporMinuto + ", RegulacionMililitros=" + RegulacionMililitros + "]";
	}
	
	
	
}
